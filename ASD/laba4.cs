using System;
					
public class Program
{
	public static void Main()
	{
		int[,] mat = new int[6, 6] {{0,3,1000,3,6,1000},
		{1000,0,4,7,1000,4},
		{3,8,0,5,1000,2},
		{1000,6,1000,0,3,1000},
		{7,1000,1,4,0,4},
		{5,2,1000,1000,2,0}};
		
 		for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                    Console.Write("{0} ", mat[i, j]);
                Console.WriteLine();
            }

        for (int k = 0; k < 6; k++)
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 6; j++)
                    if (mat[i, j] > mat[i, k] + mat[k, j])
                        mat[i, j] = mat[i, k] + mat[k, j];
 
        Console.WriteLine("Алгоритм Флойда : ");
 
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
                Console.Write("{0} ", mat[i, j]);
            Console.WriteLine();
        }
        Console.ReadLine();
    }
}